function [legend,h,lineSpec]=plot_mimo(varargin)
% PLOT_MIMO plots the contents of a NxMxF matrix on a single plot
%
%   [legend,h]=plot_mimo(Mat)
%   [legend,h]=plot_mimo(F,Mat)
%   [legend,h]=plot_mimo(M,'colspec')
%   [legend,h]=plot_mimo(F,Mat,'colspec')
%   [legend,h]=plot_mimo(F,Mat,'colspec',plotType)
%   [legend,h]=plot_mimo(F,Mat,'colspec',plotType,axisType)
%
% where Mat is an NxMxF matrix that has to be plotted. F is a vector of
% length F that's used for the X-axis. 'colspec' is information about the
% line to be used.
% plotType
%
% legend is a cell array of strings that can be used in a legend() command
% after the call of the plot_mimo function. h is the figure handle of the
% figure.
%
% Adam Cooman, ELEC VUB
% versions
% xx/xx/2013 version 1, with a lot of bugs
% 22/04/2015 version 2. This one works decently now
% 23/04/2015 added plotType and axisType options

switch nargin
    case 5
        freq = varargin{1};
        matrix = massageMatrix(varargin{2});
        col = varargin{3};
        if any(strcmpi(varargin{4},{'hold','subplot'}))
            plotType = varargin{4};
        else
            error('plotType can only be ''hold'' or ''subplot''.')
        end
        if any(strcmpi(varargin{5},{'lin','log'}))
            axisType = varargin{5};
        else
            error('plotType can only be ''lin'' or ''log''.')
        end
    case 4
        freq = varargin{1};
        matrix = massageMatrix(varargin{2});
        col = varargin{3};
        if any(strcmpi(varargin{4},{'hold','subplot'}))
            plotType = varargin{4};
        else
            error('plotType can only be ''hold'' or ''subplot''.')
        end
        axisType = 'lin';
    case 3
        freq = varargin{1};
        matrix = massageMatrix(varargin{2});
        col = varargin{3};
        plotType = 'hold';
        axisType = 'lin';
    case 2
        % either (freq,mat) or (mat,col)
        if ~isnumeric(varargin{2})
            % (mat,col)
            col = varargin{2};
            matrix = massageMatrix(varargin{1});
            F = size(matrix,3);
            freq = 1:F;
        else
            % (freq,mat)
            freq = varargin{1};
            matrix = massageMatrix(varargin{2});
            col = [];
        end
        plotType = 'hold';
        axisType = 'lin';
    case 1
        matrix = massageMatrix(varargin{1});
        F = size(matrix,3);
        freq = 1:F;
        col = [];
        plotType = 'hold';
        axisType = 'lin';
    otherwise
        error('maximum amount of input arguments is 5')
end

[N,M,~]=size(matrix);

[col,mar] = parseColSpec(col);
% in subplots, always plot with lines and in a fixed color
if strcmpi(plotType,'subplot')
    if isempty(mar)
        mar = '-';
    end
    if isempty(col)
        col='b';
    end
end
lineSpec = generateMarkerSymbols(col,mar,N*M);

% deal with the axisType
switch upper(axisType)
    case 'LIN'
        plotfun = @plot;
    case 'LOG'
        plotfun = @semilogx;
end

% if the passed data is complex, plot the nyquist plot of the FRFs
if ~isreal(matrix)
    nyquist = true;
else
    nyquist = false;
end

% now do the plottin'
legend=cell(N*M,1);
num=1;
for nn=1:N
    for mm=1:M
        % is subplots are required, make a subplot
        if strcmpi(plotType,'subplot')
            subplot(N,M,num);
        end
        % plot the data
        if nyquist
            h = plot(real(squeeze(matrix(nn,mm,:))),imag(squeeze(matrix(nn,mm,:))),lineSpec{num});
        else
            h = plotfun(freq,squeeze(matrix(nn,mm,:)),lineSpec{num});
        end
        % either hold on and add to the legend, or add a title to the subplot
        if strcmpi(plotType,'subplot')
            title([num2str(nn) num2str(mm)]);
            hold on
        else
            if (mm==1)&&(nn==1)
                hold on
            end
            legend{num}=[num2str(nn) num2str(mm)];
        end
        
        num=num+1;
    end
end

end

function res = massageMatrix(mat)
% this makes sure that the function can handle SISO vectors as well
    if isvector(mat)
        res = zeros(1,1,length(mat));
        res(1,1,:) = mat;
    else
        res = mat;
    end
end

function [color,marker] = parseColSpec(colspec)
% this function looks for a color and a marker in a provided colspec
if isempty(colspec)
    color = [];marker=[];
else
    % look for a color in the string
    color = regexp(colspec,'y|m|c|r|g|b|w|k');
    if ~isempty(color)
        color = colspec(color(1));
    else
        color=[];
    end
    % look for a marker in the string
    marker = regexp(colspec,'-|o|+|*|\.|x|s|d|\^|v|\>|\<|p|h');
    if ~isempty(marker)
        marker = colspec(marker(1));
    else
        marker=[];
    end
end
end

function res = generateMarkerSymbols(color,marker,P)
% this function generates a cell vector of P different colors and marker combinations

    if isempty(color)
        colors={'b','r','k','g','m','c','y'};
    else
        colors={color};
    end
    if isempty(marker)
        markers={'-','+','o','s','*','.','>','<','x','^','p','h'};
    else
        markers={marker};
    end
    % make all the possible combinations of these colors and markers
    combos = cell(length(colors),length(markers));
    for cc=1:length(colors)
        for mm=1:length(markers)
            combos{cc,mm} = [colors{cc} markers{mm}];
        end
    end
    % reshape into a vector
    combos = vec(combos);
    % and repeat it P times
    repeat = floor(P/length(combos));
    leftover = mod(P,length(combos));
    % repeat the combos vector enough times
    res = repmat(combos,repeat,1);
    % and add the needed leftover to the end
    res = [res;combos(1:leftover)];
end