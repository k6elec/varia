function [tf,rounded] = isnatural(x)
% checks whether all values in an array are natural numbers

rounded = round(x);
% the values should be positive
tf = all(x>=0);
if ~tf
    return
end
% check whether the values are equal to their rounded value
tf = all(rounded==x);

end