function res = squeezeDim(mat,dim)
% squeezeDim squeezes out a specific dimension from a matrix
%
%   res = squeezeDim(mat,dim)
%
% mat is a multi-dimensional matrix where at least one dimension has size 1
% dim is the dimension that will be squeezed out
%
% if mat is a [2 5 1 9] matrix and dim=3, then the result will be a [2 5 9] matrix
%
% Adam Cooman, ELEC VUB

siz = size(mat);
ndims = length(siz)-1;

% if amount of dimensions is 2, we cannot squeeze, so ignore the squeeze then
if length(siz)==2
    res = mat;
    return
end

% check whether the specified dimension can actually be squeezed out
if siz(dim)~=1
    error('cannot squeeze the selected dimension, its size is not 1');
end

% remove the unwanted dimension from the size
siz(dim)=[];
% pre-allocate the result
res = zeros(siz);

% now do the actual squeezing. It boils down to an assignment of the form
% res(:,:,...,:) = mat(:,:,...,1,...,:)
% but then with an unknown amount of dimensions, so we use subsref and subsasgn
Sasgn.type='()';
Sasgn.subs=repmat({':'},1,ndims);
Sref.type='()';
Sref.subs=repmat({':'},1,ndims+1);
Sref.subs{dim}=1;
res = subsasgn(res,Sasgn,subsref(mat,Sref));

end