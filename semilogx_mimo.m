function [legend,h]=semilogx_mimo(varargin)


switch length(varargin)
    case 3
        freq = varargin{1};
        matrix = varargin{2};
        col = varargin{3};
    case 2
        if isnumeric(varargin{2})
            col = varargin{2};
            matrix = varargin{1};
            [~,~,F] = size(matrix);
            freq = 1:F;
        else
            freq = varargin{1};
            matrix = varargin{2};
        end
    case 1
        matrix = varargin{1};
        [~,~,F] = size(matrix);
        freq = 1:F;
    otherwise
        error('maximum amount of input arguments is 3')
end



[N,M,~]=size(matrix);

if ~exist('col','var')
    nocolspec=1;
    mars={'-','+','o','s','*','.','>','<','x','^'};
    cols={'b','r','k','g','m','c'};
else
    nocolspec=0;
end

legend={};
num=0;
for nn=1:N
    for mm=1:M
        
        if nocolspec
            mar = mars{ floor(num/length(cols))+1 };
            col = cols{   mod(num,length(cols))+1 };
            h=semilogx(freq,squeeze(matrix(nn,mm,:)),[col mar]);
        else
            h=semilogx(freq,squeeze(matrix(nn,mm,:)),col);
            
        end
        
        if (mm==1)&&(nn==1)
            hold on
        end
        
        legend{end+1}=[num2str(nn) num2str(mm)];
        num=num+1;
    end
end



end