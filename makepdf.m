function makepdf(file,figuresize,h) 
% MAKEPDF exports a figure to PDF  
%  file: filename of the destination  (default: MATLAB.pdf) 
%  figuresize: [W H] vector expressed (default: [10 10]) 
%  h: figure handle                   (default: gcf)     
if nargin < 3         
	h = gcf;         
	if nargin < 2                         
		figuresize = [10 10];             
		if nargin == 0                 
			file = 'MATLAB.pdf';             
		end;         
	end;     
end;     
set(h,'PaperUnits','centimeters');     
set(h,'PaperSize',figuresize);     
set(h,'PaperPosition',[0 0 figuresize]);     
print(h,'-dpdf',file);