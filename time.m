function res=time()
% this function generates a string with the currrent time of day


t=clock();
res = [num2str(t(4)) 'h' num2str(t(5)) 'm' num2str(round(t(6))) 's' ];