function newname = copyAndRename(file,newname)
% this function makes a copy of a file and renames it
% 
%   copyAndRename(file)
%   copyAndRename(file,newname)
%   newname = copyAndRename(file)
%
% newname doesn't have to be specified, if it is unspecified, the date will
%   be added to the filename.
%
% Adam 30/05/2013:  Version 1

% split the filename into its folder and the filename
[filefolder, filename, ext] = fileparts(file);

% if no new filename is specified, make one with the current date in it.
if ~exist('newname','var')
    time = fix(clock);
    newname = [filename '_' num2str(time(1)) '_' num2str(time(2)) '_' num2str(time(3)) '_' num2str(time(4)) '_' num2str(time(5)) '_' num2str(time(6)) ext];
end

% glue the extension and the filename of the original file together
filename = [filename ext];

% split the newname into folder and filename
[newfolder, newname, ext2] = fileparts(newname);
% if the new file has no extention, add it from the other file
if isempty(ext2)
    newname = [newname ext];
else
    newname = [newname ext2];
end

% copy the old file to the new file
system(['copy "' fullfile(filefolder,filename) '" "' fullfile(newfolder,newname) '"' ])



end