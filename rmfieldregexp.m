function s = rmfieldregexp(s,exp)
% RMFIELDREGEXP removes fields from a struct which satisfy a regular expression
%
%   s = rmfieldregexp(s,exp)
%
% Where s is a struct and exp is a string that is fed to the regexp engine.
% if any occurence of the expression is found in the fieldname, the field
% is removed from the struct.
%
% Adam Cooman, ELEC VUB


f=fieldnames(s);
r = regexp(f,exp,'once');
fields = f(cellfun(@isempty,r)==0);
s = rmfield(s,fields);

end