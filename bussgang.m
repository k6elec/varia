function C = bussgang(coeff,sigma)
% calculates the gain of a static polynomial non-linearity using bussgangs
% theorem. The parameters are the coefficients of the polynomial in a
% vector and the standard deviation of the gaussian signal at the input
%
%   C = bussgang(coeff,sigma)
%
% where coeff is the vector which contains the coefficients of the polynomial
% and sigma is the standard deviation of the gaussian signal at the input
% of the non-linearity. The coefficients are ordered from low to high. 
%
% The Bussgang therorem tells us that the BLA of a static non-linearity
% when the input signal is gaussian equals a constant.
%    
%   C = 1/(sigma^3 sqrt(2*pi) ) integral u g(u) exp(-u^2/(2*sigma^2)) du
%
% where g(u) is the static non-linearity and sigma is the standard
% deviation of the gaussian distribution at the input. The integral goes
% from -infinity to infinity
% 
% If g(u) is a polynomial, we can rewrite the integral by splitting it up
% Assume g(u) = A0 + A1*u + A2*u^2 + A3*u^3 + ... 
% Then C becomes
%
%   C = 1/(sigma^3 sqrt(2*pi) ) integral u (A0 + A1*u + A2*u^2 + A3*u^3 + ... ) exp(-u^2/(2*sigma^2)) du
%   C = 1/(sigma^3 sqrt(2*pi) ) sum Ak integral u^k+1 exp(-u^2/(2*sigma^2)) du
%
% where the sum goes from k=0 up to the order of the polynomial. For even
% k, the solution to the integral is zero, which is obvious, because even
% terms don't add to the BLA. For odd terms we obtain the following
% expression:
%
%  integral u^2k exp(-u^2/(2*sigma^2)) du = (2*sigma^2)^(k+0.5) sqrt(pi) (2k-1)!!/(2^k)
%
% So, we obtain for C
%
%   C = 1/sigma^2 sum A2k-1 sigma^2k (2k-1)!!
% 
% Adam Cooman, ELEC VUB

C = 0;
for k=1:length(coeff)/2
    C = C + coeff(2*k)*sigma^(2*k)*prod(2*k-1:-2:1);
end
C = C/(sigma^2);

