function CvecM = cov_matrix(M)
% COV_MATRIX calculates the covariance of the vec of a matrix
%
%   CvecM = cov_matrix(M)
%
% where M is a 3-d matrix of size [m x n x R]. This matrix is considered as
% R measurements of a [m x n] matrix. The covariance of the vec of the
% matrix is calculated. The result is a covariance matrix of size [ m*n x m*n ]
%
% Adam Cooman, ELEC VUB

if ndims(M)~=3;error('the matrix should be a 3d matrix with the ');end

[s1,s2,m] = size(M);

Mt = zeros(s1*s2,m);
for mm=1:m
    Mt(:,mm)=vec(M(:,:,mm));
end

CvecM = cov(Mt.');

end