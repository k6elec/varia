function [leg,h] = errorbar_mimo(varargin)
% this function plots a MIMO FRM and adds the errorbars to it
%
%   errorbar_mimo(FRM,STD)
%   errorbar_mimo(freq,FRM,STD)
%   errorbar_mimo(freq,FRM,STD,colspec)
%   errorbar_mimo(freq,FRM,STD,colspec,function)
%
% inputs 
%   FRM         is an NxMxF frequency response matrix 
%   STD         is the standard deviation on each of the FRM estimates (also NxMxF). 
%   freq        is a frequency vector of length F
%   colspec     is the color and marker information for the plots (see plot_mimo)
%   function    is a function handle of a function that is applied to all elements of 
%               the FRM (usually '@db' or '@angle').  The errorbars are scaled appropriately.
%
% outputs
%   leg         legend of the plot
%   h           figure handle to the plot
%
% for the complex FRMs, the STD is assumed to give the radius of the
% uncertainty circle in the complex plane. The amplitude and phase error
% are calculated appropriately.
%
% Adam Cooman, ELEC VUB



% set the defaults
colspec = [];
fun = @(x) x;

switch nargin
    case 2 % FRM and STD
        FRM = varargin{1};
        STD = varargin{2};
    case 3 % either freq,FRM,STD or FRM,STD,colspec
        if ischar(varargin{3})
            FRM = varargin{1};
            STD = varargin{2};
            colspec = varargin{3};
        else
            freq = varargin{1};
            FRM = varargin{2};
            STD = varargin{3};
        end
    case 4 % freq,FRM,STD,colspec
        freq = varargin{1};
        FRM = varargin{2};
        STD = varargin{3};
        colspec = varargin{4};
    case 5 % freq,FRM,STD,colspec,db
        freq = varargin{1};
        FRM = varargin{2};
        STD = varargin{3};
        colspec = varargin{4};
        fun = varargin{5};
    otherwise
        error('the function can only accept 5 inputs')
end

if isvector(FRM)&&isvector(STD)
    temp_frm = FRM;clear FRM;
    temp_std = STD;clear STD;
    FRM = zeros(1,1,length(temp_frm));
    STD = zeros(1,1,length(temp_std));
    FRM(1,1,:)=temp_frm;
    STD(1,1,:)=temp_std;
end

% do some checks on the inputs
[M,N,F] = size(FRM);

funcName = func2str(fun);

if ~all([M,N,F]==size(STD))
    error('The FRM and its uncertainty should have the same dimensions')
end
if ~exist('freq','var')
    freq = (1:F).';
else 
    if ~isvector(freq)
        error('the frequency axis should be a vector')
    end
    % make sure freq is a column vector
    freq = freq(:);
    if length(freq)~=F
        error('the frequency axis should have the same length as the data');
    end
end

% look only at the abs of the variance
STD = 3*abs(STD);

%% now do the plottin'

% plot the actual data
[leg,h,lineSpec]=plot_mimo(freq,fun(FRM),colspec);

num=1;
for nn=1:N
    for mm=1:M
        % exctract the frf and its std from the matrices
        frf = squeeze(FRM(nn,mm,:));
        std = squeeze(STD(nn,mm,:));
        
        % check whether the function looks to the phase of the complex numbers
        switch funcName
            case 'angle'
                % is you are looking at the angle, calculate the uncertainty on
                % the angle when the uncertainty region is a circle aroud the
                % given value
                err = atan(std./abs(frf));
                upper = angle(frf)+err;
                lower = angle(frf)-err;
                upper(std>abs(frf)) = pi;
                lower(std>abs(frf)) = -pi;
            case 'db'
                % the db is considered. if the uncertainty is larger than the
                % amplitude, every value lower than the amplitude is viable
                upper = db(abs(frf)+std);
                lower = abs(frf)-std;
                lower(std>abs(frf)) = eps;
                lower = db(lower);
            case 'abs'
                % in the case of abs, zero is the minimum value
                upper = abs(frf)+std;
                lower = abs(frf)-std;
                lower(std>abs(frf)) = 0;
            otherwise
                % there's an unknown function (possibly abs), just plot the stuff
                upper = fun(frf+std);
                lower = fun(frf-std);
        end

        % plot the uncertainty area
        fill([freq;flipud(freq)],[upper;flipud(lower)],...
            lineSpec{num},'FaceAlpha', 0.2,'EdgeColor','none');
        
        num=num+1;
    end
end


end