function res = blkdiag_3d(varargin)
% blkdiag takes NxMxF matrices and combines them in a matrix that is blockdiagonal in the first two dimensions
%
%   res = blkdiag_3d(mat1,mat2,...,matn)
%   res = blkdiag_3d(cell)
% 
% mat1, mat2, ... matn are 3d matrices of different size in the first two
% dimensions, but all with size(X,3) equal.
% the input can also be a cell array of such matrices.
% The output is just the combined matrix where the 
% 
%               | mat1(:,:,F)  0           0    0          |
%               |  0          mat2(:,:,F)  0    0          |
%  res(:,:,F) = |  0           0          ...   0          |
%               |  0           0           0   matN(:,:,F) |
%
% Adam Cooman, ELEC VUB

% if there's only one input, it can be a matrix, put it into a cell
if nargin==1;if iscell(varargin{1});varargin = varargin{1};end;end
% check whether all fields of the cell are numeric
if ~all(cellfun(@isnumeric,varargin));error('all inputs should be numeric');end
% get the size of all the sub-fields
sizes = cell2mat(cellfun(@size,varargin,'UniformOutput',false).');
F = sizes(1,3);
% check whether every matrix has the same third dimension
if ~all(sizes(:,3)==F);error('the third dimension of all inputs should be the same');end

% preallocate the result
res = zeros(sum(sizes(:,1)),sum(sizes(:,2)),F);
% and fill it up with the matrices
loc1 = 0;
loc2 = 0;
for ii=1:length(varargin)
    res(loc1+1:loc1+sizes(ii,1),loc2+1:loc2+sizes(ii,2),:) = varargin{ii};
    loc1 = loc1+sizes(ii,1);
    loc2 = loc2+sizes(ii,2);
end


end