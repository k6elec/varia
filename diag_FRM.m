function res = diag_FRM(mat)
% DIAG_FRM allpies the diag operator to a MxNxF matrix over the first two dimensions
%
%   res = diag_FRM(mat)
%
% mat is a MxNxF matrix. 
% res is a min(M,N)xF matrix

% get the size
if ndims(mat)~=3
    error('The input should be a 3d-matrix')
end
[M,N,F]=size(mat);

% apply the diag operation over the first two dimensions
res = zeros(min(M,N),F);
for ff=1:F
    res(:,ff) = diag(mat(:,:,ff));
end

end
    